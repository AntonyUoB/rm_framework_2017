#pragma once
class CGameObject;



class CGame
{
public:
	CGame();
	~CGame(void);

	void DoFrame();
	void AddObject(CGameObject * pGameObject_);

protected:
	CGameObject * m_pRootNode;
};
