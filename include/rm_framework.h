#pragma once

// C RunTime Header Files
#include <stdlib.h>
#include <malloc.h>
#include <memory.h>
#include <tchar.h>
#include <stdio.h>
#include <math.h>
#include <gl\gl.h>
#include <gl\glu.h>
#include <fstream>

#include "Game.h"
#include "DebugLog.h"
#include "Vector.h"

#include "IRenderable.h"
#include "SkyBoxRenderable.h"
#include "Sprite.h"
#include "C3DShape.h"
#include "MeshRenderable.h"
#include "TerrainRenderable.h"

#include "IBehavior.h"
#include "GameObject.h"
#include "Input.h"
#include "Timer.h"

#include "IMaterial.h"
#include "LitMaterial.h"
#include "ColourMaterial.h"
#include "SkyBoxMaterial.h"

#include "Camera.h"
#include "Transform.h"

#include "PlayerBehavior.h"
#include "AgentBehavior.h"
#include "MissileBehavior.h"
#include "FPSCameraBehavior.h"
#include "FaceCameraBehavior.h"
#include "SpinBehavior.h"
#include "SpriteSheetAnimationBehavior.h"

#include "IMouseListener.h"
#include "MouseManager.h"
#include "TagManager.h"


#include "Graphics.h"
#include "TextureLoader.h"

#include <assert.h>
#include <map>
#include <string>
#include <vector>
using namespace std;

#define DPrint(txt) CDebugLog::Instance()->Print(txt);
#define DEG2RAD(x) 3.142*x/180