#pragma once
#include "IComponent.h"
class CGameObject;

class IRenderable: public IComponent
{
public:
	IRenderable(void);
	~IRenderable(void);

	virtual bool Render()=0; //return value of true means reset transform afterwards - Camera is only renderable to return false


};
