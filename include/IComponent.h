#pragma once
class CGameObject;
class CTransform;

class IComponent
{
public:
	IComponent(void);
	~IComponent(void);
	virtual void SetGameObject(CGameObject * pGameObject_);
	virtual void AddedToGameObject(){;}
protected:

	CGameObject * m_pGameObject;
	CTransform * m_pTransform;
};
