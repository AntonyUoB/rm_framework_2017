#pragma once

/* static functions to manage OpenGL rendering */

class CGraphics
{
public:
	CGraphics(void);
	~CGraphics(void);

	static void PrepareToRender();
	static void FinishRenderingFrame();
	static void InitializeGraphics(HWND hWnd);
	static void SetWindowSize(int width, int height);
	static void DestroyGraphicsWindow(HWND &hWnd);
	static void DrawTestObject();
	static int GetWidth(){return m_iWinWidth;}
	static int GetHeight(){return m_iWinHeight;}
	static bool b2DMode;

private:

	static bool bSetupPixelFormat();
	static HDC   s_hDC; // General HDC - (handle to device context)
	static HGLRC s_hRC; // General OpenGL_DC - Our Rendering Context for OpenGL
	static int m_iWinWidth;
	static int m_iWinHeight;


};
