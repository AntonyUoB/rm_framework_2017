#pragma once

#include <vector>
class IMouseListener;

class CMouseManager
{
public:
	static CMouseManager * Instance(); //singleton access function
	CMouseManager(void);
	~CMouseManager(void);
	void addMouseMovedListener(IMouseListener * listener_);
	void mouseMoved(float xrel_, float yrel_);

private:
	std::vector <IMouseListener * > m_vMouseListeners;
	static CMouseManager * _instance;
};

