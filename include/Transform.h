#pragma once
#include "Vector.h"
#include "icomponent.h"

class CTransform: public IComponent
{
public:
	CTransform(void);
	CTransform(float x_, float y_, float z_);
	CTransform(CVector3 * pos_);
	~CTransform(void);

	void AddedToGameObject();
	void SetPosition(CVector3 pos); //absolute
	void SetPosition(float x_, float y_, float z_);
	void Translate(CVector3 offset_); //relative
	void Translate(float x_, float y_, float z_);
	void SetSize(CVector3 size); //absolute
	void SetSize(float w_, float h_, float d_);
	CVector3 GetPosition();
	CVector3 GetSize3();
	float GetYaw(){return m_fYaw;}
	void SetYaw(float yaw_){m_fYaw = yaw_;}
	float GetPitch(){return m_fPitch;}
	void IncrementYaw(float yaw_);
	void incrementPitch(float pitch_);
	bool m_bClampToTerrain;
	float m_fClampOffset;
	void ApplyClampToTerrain();

private:
	float m_fX;
	float m_fY;
	float m_fZ;

	float m_fW;
	float m_fH;
	float m_fD;

	float m_fYaw;
	float m_fPitch;
	int m_iTextureID;
};
