#pragma once
#include "IComponent.h"

class IMaterial: public IComponent
{
public:
	IMaterial(void);
	~IMaterial(void);
	virtual void SetupRendering()=0;
	virtual void SetColour(float r_, float g_, float b_, float a_=1.0f)=0;
	virtual void SetTexture(int textureID_)=0;
};
