#include "stdafx.h"
#include "MyGame.h"


MyGame::MyGame(void)
{
	Initialize();
}


MyGame::~MyGame(void)
{
}

void
MyGame::Initialize()
{
	//load the textures

	
        CTextureLoader::getInstance().CreateTexture("assets/menu.bmp", MENU);
        CTextureLoader::getInstance().CreateTexture("assets/skybox.bmp", SKYBOX);
        CTextureLoader::getInstance().CreateTexture("assets/crate.bmp", CRATE);
        CTextureLoader::getInstance().CreateTexture("assets/fence.png", FENCE);
        CTextureLoader::getInstance().CreateTexture("assets/enemy.png", ENEMY);
		CTextureLoader::getInstance().CreateTexture("assets/hut.bmp", HUT);
		CTextureLoader::getInstance().CreateTexture("assets/grass.png", GRASS);
		CTextureLoader::getInstance().CreateTexture("assets/explosion.bmp", EXPLOSION);
		CTextureLoader::getInstance().CreateTexture("assets/walkcycle.png", WALK);
		CTextureLoader::getInstance().CreateTexture("assets/faceanim.png", FACE);
	//add some objects

	 m_pRootNode= new CGameObject();
		m_pRootNode->GetTransform()->SetPosition(0,2,5);
		m_pRootNode->SetRenderable(new CCamera());
		m_pRootNode->AddBehavior(new FPSCameraBehavior());
 
		CGameObject * pSkyBox = new CGameObject();
		pSkyBox->GetTransform()->SetPosition(1.0f,0.0f,0.0f);
		pSkyBox->GetTransform()->SetSize(50,50,50);
		pSkyBox->SetMaterial(new CSkyBoxMaterial());
		pSkyBox->GetMaterial()->SetTexture(SKYBOX);
		pSkyBox->SetRenderable(new CSkyBoxRenderable());
		m_pRootNode->AddChild(pSkyBox);

	   CGameObject * pTerrain=new CGameObject();
		pTerrain->GetTransform()->SetSize(50,1,50);
		pTerrain->SetMaterial(new LitMaterial());
        pTerrain->GetMaterial()->SetTexture(GRASS);
		m_pRootNode->AddChild(pTerrain);


        CGameObject * pFencePanel=new CGameObject();
        pFencePanel->GetTransform()->SetPosition(0,1,-5);
        pFencePanel->GetTransform()->SetSize(2,2,1);
        pFencePanel->SetRenderable(new CSprite());
        pFencePanel->SetMaterial(new CColourMaterial());
        pFencePanel->GetMaterial()->SetTexture(FENCE);
        m_pRootNode->AddChild(pFencePanel);
	

        CGameObject * pHouse=new CGameObject();
        pHouse->GetTransform()->SetPosition(-4,1.0,0);
		pHouse->SetMaterial(new LitMaterial());
        pHouse->GetTransform()->SetSize(1,1,1);
        pHouse->GetMaterial()->SetTexture(CRATE);
        m_pRootNode->AddChild(pHouse);
 
 
		CGameObject * pAlien=new CGameObject();
		pAlien->GetTransform()->SetPosition(-2, 1.0,0);
		pAlien->SetMaterial(new LitMaterial());
        pAlien->AddBehavior(new CSpinBehavior());
		pAlien->GetMaterial()->SetTexture(CRATE);
		m_pRootNode->AddChild(pAlien);



        if (1)
 
        {
 
                for (int i=0; i< 12; i++)
 
                {
 
                        CGameObject * pRMissile=new CGameObject();
						pRMissile->GetTransform()->SetSize(0.1f,0.1f,1);
                        pRMissile->GetMaterial()->SetTexture(NO_TEXTURE);
                        CMissileBehavior * pMissBh = new CMissileBehavior();
                        pMissBh->SetAngleOffset((float)i* 6.284f/12.0f );
                        pRMissile->AddBehavior(pMissBh);

                        pAlien->AddChild(pRMissile);

                }
 
        }
 

 
		CGameObject * pBillBoard=new CGameObject();
        pBillBoard->GetTransform()->SetPosition(2,1.0,0);
        pBillBoard->GetTransform()->SetSize(1,1,1);
        pBillBoard->SetRenderable(new CSprite());
        pBillBoard->SetMaterial(new CColourMaterial());
        pBillBoard->GetMaterial()->SetTexture(ENEMY);
		pBillBoard->AddBehavior(new CFaceCameraBehavior());
		m_pRootNode->AddChild(pBillBoard);

		CGameObject * pWalker=new CGameObject();
        pWalker->GetTransform()->SetPosition(2,2.0f,-4);
        pWalker->GetTransform()->SetSize(2,2,1);
        pWalker->SetRenderable(new CSprite());
        pWalker->SetMaterial(new CColourMaterial());
        pWalker->GetMaterial()->SetTexture(ENEMY);
		pWalker->AddBehavior(new CFaceCameraBehavior());
        m_pRootNode->AddChild(pWalker);

		
		CGameObject * pExplosion=new CGameObject();
        pExplosion->GetTransform()->SetPosition(-4,2.0f,-4);
        pExplosion->GetTransform()->SetSize(2,2,1);
        pExplosion->SetRenderable(new CSprite());
        pExplosion->SetMaterial(new CColourMaterial());
        pExplosion->GetMaterial()->SetTexture(ENEMY);
		pExplosion->AddBehavior(new CFaceCameraBehavior());
        m_pRootNode->AddChild(pExplosion);
}