#include "stdafx.h"
#include "Camera.h"
#define DEG2RAD(x) 3.142*x/180

CCamera::CCamera(void)
{

}


CCamera::~CCamera(void)
{
}

void
CCamera::AddedToGameObject()
{
	//tag this object with 'Camera'
	CTagManager::Instance()->AddTag(m_pGameObject,"Camera");
}

bool 
CCamera::Render()
{
	//calculate new lookat based on current Yaw,pitch roll
	float yaw=m_pTransform->GetYaw();
	float pitch=m_pTransform->GetPitch();
	float x=m_pTransform->GetPosition().m_fX;
	float y=m_pTransform->GetPosition().m_fY;
	float z=m_pTransform->GetPosition().m_fZ;
	float cosYaw,sinYaw,sinPitch;
	cosYaw=(float)cos(DEG2RAD(yaw));
	sinYaw=(float)sin(DEG2RAD(yaw));
	sinPitch=(float)sin(DEG2RAD(pitch));

	//update lookat based on yaw and pitch
	float lookAtx=x+sinYaw;
	float lookAty=y+sinPitch;
	float lookAtz=z-cosYaw;

	gluLookAt(x,y,z,lookAtx, lookAty, lookAtz,0,1,0);

	//glPushMatrix();
	return false; //tells recursive render function not to reset transform - this is the only time to do that
}


