#include "stdafx.h"
#include "FPSCameraBehavior.h"
#include "Input.h"

FPSCameraBehavior::FPSCameraBehavior(void)
{

	//register self as Mouse Listener
	CMouseManager::Instance()->addMouseMovedListener(this);
}


FPSCameraBehavior::~FPSCameraBehavior(void)
{
}

void
FPSCameraBehavior::Update(float dt_)
{

	if (CInput::Instance()->GetIfKeyDown(DIK_D))
	{
		m_fStrafeSpeed=10;
	}
	else if (CInput::Instance()->GetIfKeyDown(DIK_A))
	{
		m_fStrafeSpeed=-10;
	
	}
	else m_fStrafeSpeed=0.0f;

	if (CInput::Instance()->GetIfKeyDown(DIK_W))
	{
		m_fSpeed=10;
		
	}
	else if (CInput::Instance()->GetIfKeyDown(DIK_S))
	{
		m_fSpeed=-10;
		
	}
	else m_fSpeed=0.0f;


	float cosYaw,sinYaw,sinPitch;
	cosYaw=(float)cos(DEG2RAD(m_pTransform->GetYaw()));
	sinYaw=(float)sin(DEG2RAD(m_pTransform->GetYaw()));
	//sinPitch=(float)sin(DEG2RAD(m_pTransform->GetPitch()));

	//update position based on speed
	float dx=sinYaw*m_fSpeed*dt_ +cosYaw*m_fStrafeSpeed*dt_;
	float dz=-cosYaw*m_fSpeed*dt_ + sinYaw*m_fStrafeSpeed*dt_;
	float dy=0;
	m_pTransform->Translate(dx,dy,dz);

	//m_pTransform->Translate(CVector3(0,0,-0.1));
}


void 
FPSCameraBehavior::MouseMoved(float x_, float y_)
{
	m_pTransform->IncrementYaw(x_ *15);
	m_pTransform->incrementPitch(y_ *-15);
}