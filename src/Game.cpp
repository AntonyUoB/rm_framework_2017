#include "StdAfx.h"
#include "Game.h"

CGame::CGame(void)
{
	m_pRootNode=0;
	
}

CGame::~CGame(void)
{
}




void
CGame::AddObject(CGameObject * pGameObject_)
{
m_pRootNode->AddChild(pGameObject_);
}


void
CGame::DoFrame()
{
	//refresh input
	CInput::Instance()->GetInput();
	float dt=CTimer::Instance()->Getdt();

	//update all objects
	if (m_pRootNode)
	{
	m_pRootNode->UpdateRecursive(dt);
	
	//render all objects
	m_pRootNode->RenderRecursive();
	}
}
