#include "StdAfx.h"
#include "GameObject.h"
#include <gl\gl.h>

CGameObject::CGameObject(void)
{
	m_pRenderable=0;
	m_pMaterial=0;
	//m_pBehavior=0;

	//add default components
	SetMaterial(new LitMaterial());
	SetTransform(new CTransform());
	SetRenderable(new C3DShape());

}

CGameObject::~CGameObject(void)
{
}




bool
CGameObject::Render()
{
	if (m_pMaterial)
	{
		m_pMaterial->SetupRendering();
	}

	if (m_pRenderable)
	{
		return m_pRenderable->Render();
	}
	
}

void
CGameObject::Update(float dt_)
{
	/*if (m_pBehavior)
	{
		m_pBehavior->Update(dt_);
	}*/

		//loop through all behaviors
		std::vector<IBehavior *>::iterator itr = m_vBehaviors.begin();
		for (; itr<m_vBehaviors.end(); itr++)
		{
		(*itr)->Update(dt_);
		}
}

void
CGameObject::RenderRecursive()
{
	//render self then render children

	glPushMatrix();
	Render();

	if (HasChild())
	{
		((CGameObject *)pChild)->RenderRecursive();
	}

	glPopMatrix();
	
	//then render siblings

	if (HasParent() && !IsLastChild())
	{
	((CGameObject *)pNext)->RenderRecursive();
	}
}

void
CGameObject::UpdateRecursive(float dt_)
{

	//update self then update children


	Update(dt_);

	if (HasChild())
	{
		((CGameObject *)pChild)->UpdateRecursive(dt_);
	}

	
	//then render siblings

	if (HasParent() && !IsLastChild())
	{
	((CGameObject *)pNext)->UpdateRecursive(dt_);
	}
}

void 
CGameObject::SetMaterial(IMaterial * pMaterial_)
{
	m_pMaterial=pMaterial_;
	m_pMaterial->SetGameObject(this);
}

void 
CGameObject::SetRenderable(IRenderable * pRenderable_)
{
	m_pRenderable=pRenderable_;
	m_pRenderable->SetGameObject(this);
}

/*void
CGameObject::SetBehavior(IBehavior * pBehavior_)
{
	m_pBehavior=pBehavior_;
	m_pBehavior->SetGameObject(this);
}*/

void
CGameObject::AddBehavior(IBehavior * pBehavior_)
{
	m_vBehaviors.push_back(pBehavior_);
	pBehavior_->SetGameObject(this);
}

void
CGameObject::SetTransform(CTransform * pTransform_)
{
	m_pTransform=pTransform_;

}


IRenderable * 
CGameObject::GetRenderable()
{
	return m_pRenderable;
}

IMaterial * 
CGameObject::GetMaterial()
{
	return m_pMaterial;
}

/*IBehavior * 
CGameObject::GetBehavior()
{
	return m_pBehavior;
}*/

CTransform * 
CGameObject::GetTransform()
{
	return m_pTransform;
}