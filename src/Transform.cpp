#include "StdAfx.h"
#include "Transform.h"

CTransform::CTransform(void)
{
	m_fX=0.0f;
	m_fY=0.0f;
	m_fZ=0.0f;

	m_fW=1.0f;
	m_fH=1.0f;
	m_fD=1.0f;

	m_fYaw=0.0f;
	m_fPitch=0.0f;

	m_bClampToTerrain=false;
	m_fClampOffset=0.0f;
}

CTransform::CTransform(CVector3 * pos_)
{
	m_fX=pos_->m_fX;
	m_fY=pos_->m_fY;
	m_fZ=pos_->m_fZ;

	m_fW=1.0f;
	m_fH=1.0f;
	m_fD=1.0f;

	m_fYaw=0.0f;
	m_fPitch=0.0f;

	m_bClampToTerrain=false;
	m_fClampOffset=0.0f;
}


CTransform::CTransform(float x_, float y_, float z_)
{
	m_fX=x_;
	m_fY=y_;
	m_fZ=z_;

	m_fW=1.0f;
	m_fH=1.0f;
	m_fD=1.0f;

	m_fYaw=0.0f;
	m_fPitch=0.0f;
}

CTransform::~CTransform(void)
{
}

void 
CTransform::SetPosition(CVector3 pos)
{
	m_fX=pos.m_fX;
	m_fY=pos.m_fY;
	m_fZ=pos.m_fZ;

}

void 
CTransform::SetPosition(float x_, float y_, float z_)
{
	m_fX=x_;
	m_fY=y_;
	m_fZ=z_;
}

void 
CTransform::SetSize(CVector3 scale)
{
	m_fW=scale.m_fX;
	m_fH=scale.m_fY;
	m_fD=scale.m_fZ;

}

void
CTransform::SetSize(float w_, float h_, float d_)
{
	m_fW=w_;
	m_fH=h_;
	m_fD=d_;
}

CVector3 
CTransform::GetPosition()
{
	CVector3 out;
	out.m_fX=m_fX;
	out.m_fY=m_fY;
	out.m_fZ=m_fZ;
	return out;
}

CVector3 
CTransform::GetSize3()
{
	CVector3 out;
	out.m_fX=m_fW;
	out.m_fY=m_fH;
	out.m_fZ=m_fD;
	return out;
}


void
CTransform::Translate(CVector3 offset_)
{
	m_fX+=offset_.m_fX;
	m_fY+=offset_.m_fY;
	m_fZ+=offset_.m_fZ;
}

void 
CTransform::Translate(float x_, float y_, float z_)
{
	m_fX+=x_;
	m_fY+=y_;
	m_fZ+=z_;
}

void	 
CTransform::IncrementYaw(float yaw_)
{
	m_fYaw +=yaw_;
	if (m_fYaw > 360) m_fYaw-=360;
}

	
void 
CTransform::incrementPitch(float pitch_)
{
	m_fPitch +=pitch_;
	if(m_fPitch > 360) m_fPitch -=360;
}

void
CTransform::ApplyClampToTerrain()
{
	if (!m_bClampToTerrain) return;
	
	//get Terrain
	CGameObject * obj =CTagManager::Instance()->GetObjectWithTag("Terrain");

	if (obj)
	{
		CTerrainRenderable * tern = (CTerrainRenderable *) obj->GetRenderable();
		float y = tern ->GetHeightAtPoint(m_fX,m_fZ);
		m_fY =y + m_fClampOffset;
	}
}


void
CTransform::AddedToGameObject()
{
//apply terrain clamping
	ApplyClampToTerrain();
}