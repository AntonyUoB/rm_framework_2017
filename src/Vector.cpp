#include "stdafx.h"
#include "Vector.h"
#include <math.h>

CVector3::CVector3(float x_,float y_, float z_)
{
	m_fX=x_;
	m_fY=y_;
	m_fZ=z_;
}

CVector3::CVector3()
{

}

CVector3::~CVector3(void)
{
}

void CVector3::AddVector(CVector3 * vec_)
{
	m_fX += vec_->m_fX;
	m_fY += vec_->m_fY;
	m_fZ += vec_->m_fZ;
}

void
CVector3::Normalise()
{
float length=this->GetLength();
m_fX/=length;
m_fY/=length;
m_fZ/=length;
}

float
CVector3::DotProduct(CVector3 * invector)
{
return this->m_fX * invector->m_fX + this->m_fY*invector->m_fY + this->m_fZ*invector->m_fZ;
}

CVector3 
CVector3::operator-(CVector3 op2) 
{ 
  CVector3 temp; 
 
  temp.m_fX = m_fX - op2.m_fX; 
  temp.m_fY = m_fY - op2.m_fY; 
  temp.m_fZ = m_fZ - op2.m_fZ; 
  return temp; 
}

CVector3 
CVector3::operator+(CVector3 op2) 
{ 
  CVector3 temp; 
 
  temp.m_fX = m_fX + op2.m_fX; 
  temp.m_fY = m_fY + op2.m_fY; 
  temp.m_fZ = m_fZ + op2.m_fZ; 
  return temp; 
}

CVector3 
CVector3::operator*(float mult) 
{ 
  CVector3 temp; 
 
  temp.m_fX = m_fX *mult;
  temp.m_fY = m_fY *mult;
  temp.m_fZ = m_fZ*mult;
  return temp; 
}

float
CVector3::GetLength()
{
return sqrt(1+m_fX*m_fX+m_fY*m_fY+m_fZ*m_fZ);
}


CVector2::CVector2(float x_,float y_)
{
	m_fX=x_;
	m_fY=y_;

}

CVector2::CVector2()
{

}

CVector2::~CVector2(void)
{
}


void
CVector2::Normalise()
{
float length=this->GetLength();
m_fX/=length;
m_fY/=length;

}

float
CVector2::DotProduct(CVector2 * invector)
{
return this->m_fX * invector->m_fX + this->m_fY*invector->m_fY;
}

CVector2 
CVector2::operator-(CVector2 op2) 
{ 
  CVector2 temp; 
 
  temp.m_fX = m_fX - op2.m_fX; 
  temp.m_fY = m_fY - op2.m_fY; 

  return temp; 
}

CVector2 
CVector2::operator+(CVector2 op2) 
{ 
  CVector2 temp; 
 
  temp.m_fX = m_fX + op2.m_fX; 
  temp.m_fY = m_fY + op2.m_fY; 

  return temp; 
}

CVector2 
CVector2::operator*(float mult) 
{ 
  CVector2 temp; 
 
  temp.m_fX = m_fX *mult;
  temp.m_fY = m_fY *mult;

  return temp; 
}

float
CVector2::GetLength()
{
return sqrt(m_fX*m_fX+m_fY*m_fY);
}