#include "stdafx.h"
#include "FireMissileBehavior.h"
extern CSpaceGame * g_pGame;

CFireMissileBehavior::CFireMissileBehavior(void)
{
}


CFireMissileBehavior::~CFireMissileBehavior(void)
{
}

void
CFireMissileBehavior::Update(float dt_)
{

	if (CInput::Instance()->GetIfKeyDownEvent(DIK_SPACE))
	{
		//FIRE MISSILE
		CGameObject * pMissile=new CGameObject();
        pMissile->GetTransform()->SetPosition(m_pGameObject->GetTransform()->GetPosition().m_fX,m_pGameObject->GetTransform()->GetPosition().m_fY,m_pGameObject->GetTransform()->GetPosition().m_fZ);
        pMissile->GetTransform()->SetSize(0.1,0.2,1);
        pMissile->SetRenderable(new CSprite());
        pMissile->SetMaterial(new CColourMaterial());
        pMissile->GetMaterial()->SetTexture(2);
		pMissile->AddBehavior(new CMissileBehavior());

		g_pGame->AddObject(pMissile);

	}

}